package main;

/**
 *
 * @author Edson
 */
public class Pizza {
    
    private int cantidadRebanadas;

    public Pizza(int cantidadRebanadas) {
        this.cantidadRebanadas = cantidadRebanadas;
    }

    @Override
    public String toString() {
        return "Pizza{" + "Cantidad Rebanadas: " + this.cantidadRebanadas + '}';
    }
    
    
}
